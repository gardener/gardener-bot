package main

import (
	"os"

	"codeberg.org/gardener/gardener-bot/cmd/gardener/bot"
	"codeberg.org/gardener/gardener-bot/internal"
	"codeberg.org/gardener/gardener-bot/internal/log"
	"github.com/urfave/cli/v2"
)

var app = &cli.App{
	Name:    "The Gardener",
	Usage:   "yet another discord bot",
	Version: internal.Version,

	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:    "database",
			Aliases: []string{"db"},
			Usage:   "Connection string for the database",
			EnvVars: []string{"GARDENER_DATABASE"},
			Value:   "",
		},
	},

	Commands: []*cli.Command{
		bot.Command,
	},
}

func main() {
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
