package bot

import (
	"context"

	"codeberg.org/gardener/gardener-bot/internal/db"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
)

type Database interface {
	Migrator() Migrator
	Scheduler() Scheduler

	Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...any) pgx.Row
	Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)

	GetContext(ctx context.Context, v any, sql string, args ...any) error
	SelectContext(ctx context.Context, v any, sql string, args ...any) error
	Get(v any, sql string, args ...any) error
	Select(v any, sql string, args ...any) error

	db.Config
}

type SchedulerBot = db.Bot
type Event = db.Event

type Migrator = db.MigratorInterface
type Scheduler = db.SchedulerInterface

var _ Database = (*db.DB)(nil)
var _ Migrator = (*db.Migrator)(nil)
var _ Scheduler = (*db.Scheduler)(nil)
