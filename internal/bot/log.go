package bot

import "codeberg.org/gardener/gardener-bot/internal/log"

func (*Bot) Logger() Logger {
	return logger
}

type Logger interface {
	Debug(v ...any)
	Info(v ...any)
	Warn(v ...any)
	Error(v ...any)
	Fatal(v ...any)

	Debugf(tmpl string, v ...any)
	Infof(tmpl string, v ...any)
	Warnf(tmpl string, v ...any)
	Errorf(tmpl string, v ...any)
	Fatalf(tmpl string, v ...any)
}

var logger Logger = globalLogger{}

type globalLogger struct{}

func (globalLogger) Debug(v ...any) {
	log.SugaredLogger.Debug(v...)
}

func (globalLogger) Info(v ...any) {
	log.SugaredLogger.Info(v...)
}

func (globalLogger) Warn(v ...any) {
	log.SugaredLogger.Warn(v...)
}

func (globalLogger) Error(v ...any) {
	log.SugaredLogger.Error(v...)
}

func (globalLogger) Fatal(v ...any) {
	log.SugaredLogger.Fatal(v...)
}

func (globalLogger) Debugf(tmpl string, v ...any) {
	log.SugaredLogger.Debugf(tmpl, v...)
}

func (globalLogger) Infof(tmpl string, v ...any) {
	log.SugaredLogger.Infof(tmpl, v...)
}

func (globalLogger) Warnf(tmpl string, v ...any) {
	log.SugaredLogger.Warnf(tmpl, v...)
}

func (globalLogger) Errorf(tmpl string, v ...any) {
	log.SugaredLogger.Errorf(tmpl, v...)
}

func (globalLogger) Fatalf(tmpl string, v ...any) {
	log.SugaredLogger.Fatalf(tmpl, v...)
}
