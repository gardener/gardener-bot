package bot

import (
	"codeberg.org/gardener/gardener-bot/internal/log"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/diamondburned/arikawa/v3/gateway"
)

func (bot *Bot) ready(ev *gateway.ReadyEvent) {
	log.Debugf("Ready as %v / %v!", ev.User.Tag(), ev.User.ID)

	bot.readyOnce.Do(func() {
		go bot.setupCommands(ev)

		for _, m := range bot.Modules {
			go m.Ready()
		}
	})
}

func (bot *Bot) setupCommands(ev *gateway.ReadyEvent) {
	if !bot.Config.Discord.OverwriteCommands {
		log.Debug("Not overwriting commands")
		return
	}

	if bot.Config.Discord.CommandsGuild.IsValid() {
		log.Debugf("Overwriting commands in guild %v", bot.Config.Discord.CommandsGuild)

		_, err := bot.State.BulkOverwriteGuildCommands(discord.AppID(ev.User.ID), bot.Config.Discord.CommandsGuild, bot.appCommands)
		if err != nil {
			log.Errorf("overwriting commands in %v: %v", bot.Config.Discord.CommandsGuild, err)
			return
		}
	} else {
		log.Debug("Overwriting commands globally")

		_, err := bot.State.BulkOverwriteCommands(discord.AppID(ev.User.ID), bot.appCommands)
		if err != nil {
			log.Errorf("overwriting commands globally: %v", err)
			return
		}
	}

	log.Debug("Overwrote commands!")
}
