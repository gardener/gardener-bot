package bot

import (
	"context"
	"fmt"
	"sync"

	"codeberg.org/gardener/gardener-bot/internal/config"
	"codeberg.org/gardener/gardener-bot/internal/db"
	"codeberg.org/gardener/gardener-bot/internal/log"
	"emperror.dev/errors"
	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/gateway"
	"github.com/diamondburned/arikawa/v3/state"
	"github.com/starshine-sys/bcr/v2"
)

type Bot struct {
	Config config.Config

	State        *state.State
	DB           Database
	Interactions *bcr.Router

	Modules      []Module
	modulesSetUp bool
	appCommands  []api.CreateCommandData

	readyOnce sync.Once
}

const intents = gateway.IntentGuildBans |
	gateway.IntentGuildEmojis |
	gateway.IntentGuildIntegrations |
	gateway.IntentGuildInvites |
	gateway.IntentGuildMessageReactions |
	gateway.IntentGuildMessages |
	gateway.IntentGuildPresences |
	gateway.IntentGuildVoiceStates |
	gateway.IntentGuildWebhooks |
	gateway.IntentGuilds |
	gateway.IntentGuildMembers |
	gateway.IntentDirectMessageReactions |
	gateway.IntentDirectMessages

func New(c config.Config) (bot *Bot, err error) {
	bot = &Bot{Config: c}
	bot.DB, err = db.New(c.Auth.Database)
	if err != nil {
		return nil, errors.Wrap(err, "connecting to database")
	}

	id := gateway.NewIdentifier(gateway.IdentifyCommand{
		Token: "Bot " + c.Auth.Token,
		Properties: gateway.IdentifyProperties{
			Browser: "Discord iOS",
		},
	})
	id.AddIntents(intents)

	bot.State = state.NewWithIdentifier(id)

	// set up interactions
	bot.Interactions = bcr.NewFromState(bot.State)
	bot.State.AddHandler(bot.interactionCreate)

	return bot, nil
}

// Start sets up all of the registered modules, and opens a connection to Discord.
func (bot *Bot) Start(ctx context.Context) error {
	if !bot.modulesSetUp {
		for _, m := range bot.Modules {
			// set the module's Bot
			m.init(bot)

			// unmarshal the config, if it exists
			err := readModuleConfig(m)
			if err != nil {
				return errors.Wrap(err, fmt.Sprintf("reading config for module %q", m.Name()))
			}

			// add the module's migrations
			if m.Migrations() != nil {
				err := bot.DB.Migrator().AddMigrations(m.Name(), m.Migrations())
				if err != nil {
					return errors.Wrap(err, fmt.Sprintf("adding migrations for module %q", m.Name()))
				}

				log.Infof("Added migrations for module %q", m.Name())
			}

			// add the module's scheduler types
			bot.DB.Scheduler().AddType(m.EventTypes()...)

			// add the module's handlers
			for _, hn := range m.Handlers() {
				bot.State.AddHandler(hn)
			}

			// add the module's application command definitions
			if commands := m.AppCommands(); commands != nil {
				bot.appCommands = append(bot.appCommands, commands...)
			}

			err = m.Setup()
			if err != nil {
				return errors.Wrap(err,
					fmt.Sprintf("setting up module %q", m.Name()))
			}
		}
	}

	// run migrations
	err := bot.DB.Migrator().Migrate()
	if err != nil {
		return errors.Wrap(err, "running migrations")
	}

	// add bot handlers
	bot.State.AddHandler(bot.ready)

	// start scheduler
	go bot.DB.Scheduler().(*db.Scheduler).Start(&db.Bot{
		State: bot.State,
		DB:    bot.DB.(*db.DB),
	})

	err = bot.State.Open(ctx)
	if err != nil {
		return errors.Wrap(err, "opening state")
	}
	return nil
}
