package bot

import (
	"codeberg.org/gardener/gardener-bot/internal/log"
	"github.com/diamondburned/arikawa/v3/gateway"
)

// interactionCreate handles interaction commands and components.
func (bot *Bot) interactionCreate(ev *gateway.InteractionCreateEvent) {
	if err := bot.Interactions.Execute(ev); err != nil {
		log.Errorf("executing interaction %v: %v", ev.ID, err)
	}
}
