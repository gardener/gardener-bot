package db

import (
	"time"

	migrate "github.com/rubenv/sql-migrate"
)

type MigratorInterface interface {
	AddMigrations(schema string, src migrate.MigrationSource) error
	Migrate() error
}

type SchedulerInterface interface {
	AddType(events ...Event)
	Add(t time.Time, v Event) (id int64, err error)
	Remove(id int64) error
	Reschedule(id int64, dur time.Duration) error
}
