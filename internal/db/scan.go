package db

import (
	"context"

	"github.com/georgysavva/scany/pgxscan"
)

func (db *DB) GetContext(ctx context.Context, v any, sql string, args ...any) error {
	return pgxscan.Get(ctx, db, v, sql, args...)
}

func (db *DB) SelectContext(ctx context.Context, v any, sql string, args ...any) error {
	return pgxscan.Select(ctx, db, v, sql, args...)
}

func (db *DB) Get(v any, sql string, args ...any) error {
	return db.GetContext(context.Background(), v, sql, args...)
}

func (db *DB) Select(v any, sql string, args ...any) error {
	return db.SelectContext(context.Background(), v, sql, args...)
}
