-- +migrate Up

-- 2022-02-04
-- Add user, guild, and guild-scope user configuration stores
-- NOTE: if you're running a PostgreSQL version pre-13, and the user you're connecting with is not a superuser, you must run `CREATE EXTENSION hstore;` on the database manually *BEFORE* running this migration.

-- +migrate StatementBegin
DO $$
DECLARE
version integer := (regexp_match(version(), 'PostgreSQL (\d+)\.*'))[1];
superuser boolean := (select usesuper from pg_user where usename = CURRENT_USER);
BEGIN
IF coalesce(version >= 13, true) OR superuser THEN
    create extension if not exists hstore;
END IF;
END $$;
-- +migrate StatementEnd

create table user_config (
    user_id bigint  primary key,
    config  hstore  not null default ''
);

create table guild_config (
    guild_id    bigint  primary key,
    config      hstore  not null default ''
);

create table user_guild_config (
    user_id     bigint  not null,
    guild_id    bigint  not null,
    config      hstore  not null default '',

    primary key (guild_id, user_id)
);
