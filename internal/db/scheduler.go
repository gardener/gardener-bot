package db

import (
	"context"
	"encoding/json"
	"os"
	"os/signal"
	"reflect"
	"sync"
	"time"

	"codeberg.org/gardener/gardener-bot/internal/log"
	"emperror.dev/errors"
	"github.com/Masterminds/squirrel"
	"github.com/diamondburned/arikawa/v3/state"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Event is any event that can be scheduled.
// Execute is called when the event is due to fire, Offset is called to determine how much to move the event.
type Event interface {
	Execute(ctx context.Context, id int64, bot *Bot) error
	Offset() time.Duration
}

// Return Reschedule if the event should be rescheduled (offset by the duration returned from Offset)
const Reschedule = errors.Sentinel("reschedule event")

type Scheduler struct {
	*pgxpool.Pool

	started bool

	bot    *Bot
	mu     sync.RWMutex
	events map[string]func() Event
}

type Bot struct {
	DB    *DB
	State *state.State
}

func NewScheduler(p *pgxpool.Pool) *Scheduler {
	return &Scheduler{
		Pool:   p,
		events: map[string]func() Event{},
	}
}

// AddType adds event types.
// These should be *pointers*, anything else will panic, even if it implements Event!
func (s *Scheduler) AddType(events ...Event) {
	s.mu.Lock()
	for _, v := range events {
		t := reflect.ValueOf(v).Elem().Type()

		log.Infof("Adding type %q to scheduler", t.String())

		s.events[t.String()] = func() Event {
			return reflect.New(t).Interface().(Event)
		}
	}
	s.mu.Unlock()
}

func (s *Scheduler) Add(t time.Time, v Event) (id int64, err error) {
	typ := reflect.ValueOf(v).Elem().Type()

	s.mu.RLock()
	_, ok := s.events[typ.String()]
	s.mu.RUnlock()
	if !ok {
		return 0, ErrUnknownEvent
	}

	log.Debugf("Scheduling event type %q for %v", typ.String(), t)

	b, err := json.Marshal(v)
	if err != nil {
		return 0, errors.Wrap(err, "marshal json")
	}

	sql, args, err := squirrel.Insert("public.scheduled_events").
		Columns("event_type", "expires", "data").
		Values(typ.String(), t.UTC(), b).
		Suffix("RETURNING id").
		ToSql()
	if err != nil {
		return 0, errors.Wrap(err, "build query")
	}

	return id, s.QueryRow(context.Background(), sql, args...).Scan(&id)
}

func (s *Scheduler) Remove(id int64) error {
	sql, args, err := squirrel.Delete("public.scheduled_events").Where(squirrel.Eq{"id": id}).ToSql()
	if err != nil {
		return errors.Wrap(err, "build query")
	}

	_, err = s.Exec(context.Background(), sql, args...)
	return err
}

func (s *Scheduler) Reschedule(id int64, dur time.Duration) error {
	sql, args, err := squirrel.Update("public.scheduled_events").Set("expires", time.Now().UTC().Add(dur)).Where(squirrel.Eq{"id": id}).ToSql()
	if err != nil {
		return errors.Wrap(err, "build query")
	}

	_, err = s.Exec(context.Background(), sql, args...)
	return err
}

type row struct {
	ID        int64
	EventType string
	Expires   time.Time
	Data      json.RawMessage
}

func (s *Scheduler) expiring(ctx context.Context) ([]row, error) {
	var rs []row

	sql, _, err := squirrel.Select("*").From("public.scheduled_events").Where("expires < current_timestamp at time zone 'utc'").OrderBy("id").Limit(5).ToSql()
	if err != nil {
		return nil, err
	}

	err = pgxscan.Select(ctx, s, &rs, sql)
	return rs, errors.Cause(err)
}

// Start starts the scheduler. *This function is blocking!*
func (s *Scheduler) Start(bot *Bot) {
	s.mu.Lock()
	if s.started {
		log.Warnf("Scheduler.Start called after the scheduler was already started")
		s.mu.Unlock()
		return
	}
	s.started = true
	s.bot = bot
	s.mu.Unlock()

	log.Infof("Starting scheduler")

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-ctx.Done():
			log.Info("Stopping scheduler")
			return

		case <-ticker.C:
		}

		err := s.tick(ctx)
		if err != nil {
			log.Errorf("Error running scheduler tick: %v", err)
		}
	}
}

func (s *Scheduler) tick(ctx context.Context) error {
	rs, err := s.expiring(ctx)
	if err != nil {
		return err
	}

	for _, r := range rs {
		dur, err := s.run(ctx, r)
		if err != nil {
			if err == Reschedule {
				err = s.Reschedule(r.ID, dur)
				if err != nil {
					return errors.Wrap(err, "rescheduling event")
				}
			} else {
				err = s.Remove(r.ID)
				if err != nil {
					return errors.Wrap(err, "removing errored event")
				}
			}
			continue
		}

		// otherwise, remove the event, as it's done
		err = s.Remove(r.ID)
		if err != nil {
			return errors.Wrap(err, "removing completed event")
		}
	}
	return nil
}

const ErrUnknownEvent = errors.Sentinel("unknown event type")

func (s *Scheduler) run(ctx context.Context, r row) (offset time.Duration, err error) {
	var ev Event
	s.mu.RLock()
	fn, ok := s.events[r.EventType]
	s.mu.RUnlock()
	if !ok {
		return 0, ErrUnknownEvent
	}
	ev = fn()

	err = json.Unmarshal(r.Data, ev)
	if err != nil {
		return 0, errors.Wrap(err, "unmarshaling json")
	}

	cctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	err = ev.Execute(cctx, r.ID, s.bot)
	if err != nil {
		return ev.Offset(), err
	}
	return 0, nil
}
