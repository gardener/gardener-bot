package db

import (
	"database/sql"
	"fmt"

	"codeberg.org/gardener/gardener-bot/internal/log"
	"emperror.dev/errors"
	migrate "github.com/rubenv/sql-migrate"

	// pgx driver for migrations
	_ "github.com/jackc/pgx/v4/stdlib"
)

// Migrator manages database migrations.
// Each module should use its own separate database schema, to avoid conflicting with other modules.
// All modules should use the database independently from each other (no cross-schema relations), even though they use the same connection pool.
type Migrator struct {
	connString string
	migrations []migration
}

type migration struct {
	schema string
	src    migrate.MigrationSource
}

const ErrSchemaAlreadyExists = errors.Sentinel("a schema with that name already has migrations added")

func (m *Migrator) AddMigrations(schema string, src migrate.MigrationSource) error {
	for _, m := range m.migrations {
		if m.schema == schema {
			return ErrSchemaAlreadyExists
		}
	}

	m.migrations = append(m.migrations, migration{
		schema: schema,
		src:    src,
	})
	return nil
}

func (m *Migrator) Migrate() error {
	db, err := sql.Open("pgx", m.connString)
	if err != nil {
		return errors.Wrap(err, "m.Migrate: connect to database")
	}

	if err := db.Ping(); err != nil {
		return errors.Wrap(err, "m.Migrate: ping database")
	}

	for _, m := range m.migrations {
		log.Debugf("Running migrations for schema %q", m.schema)

		migrate.SetSchema(m.schema)
		migrate.SetTable("migrations")

		n, err := migrate.Exec(db, "postgres", m.src, migrate.Up)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("m.Migrate: migrate schema %q", m.schema))
		}
		if n != 0 {
			log.Infof("Performed %d migration(s) for schema %q", n, m.schema)
		}
	}
	return nil
}
