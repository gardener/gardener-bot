package internal

import (
	"os/exec"
	"strings"

	"codeberg.org/gardener/gardener-bot/internal/log"
)

var Version = ""

func init() {
	if Version == "" {
		log.Info("Version not set, falling back to checking current directory.")

		git := exec.Command("git", "rev-parse", "--short", "HEAD")
		// ignoring errors *should* be fine? if there's no output we just fall back to "unknown"
		b, _ := git.Output()
		Version = strings.TrimSpace(string(b))
		if Version == "" {
			Version = "[unknown]"
		}
	}
}

// A couple of frequently used embed colours
const (
	ColourBlurple = 0x7289da
	ColourPurple  = 0x9b59b6
	ColourGold    = 0xf1c40f
	ColourOrange  = 0xe67e22
	ColourRed     = 0xe74c3c
	ColourGreen   = 0x2ecc71
	ColourBlue    = 0x3498db
)
