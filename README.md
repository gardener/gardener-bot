# gardenerbot

hopefully a relatively easyish to maintain alternative discord-bot with less spaghetti than our existing ones

## requirements

- Go 1.17
- PostgreSQL 12 or later (latest version recommended, may work with versions <12 but not tested)

## running

this bot doesn't do much so far  
to run it in its current state, create a file named `gardener.yaml` in the `config/` directory, and add the following:

```yaml
auth:
  token: discordTokenHere
  database: postgresql://postgres:postgres@localhost/postgres
```

replacing `token` with your bot's discord token and `database` with your database url

if you're using PostgreSQL 12 or earlier, and don't have the bot connect as a superuser,
you will have to run `CREATE EXTENSION hstore;` as superuser on the database you created *before* running the bot for the first time  
(because hstore is not a trusted module before PostgreSQL 13)
