package report

import (
	"context"
	"fmt"
	"time"

	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/starshine-sys/bcr/v2"
)

func (m *Module) report(ctx *bcr.CommandContext) (err error) {
	chID, err := m.DB.GetGuildInt(context.Background(), ctx.Event.GuildID, "report:channel")
	if err != nil || chID == 0 {
		return ctx.ReplyEphemeral("This guild is not configured for reports.")
	}

	successMsg, err := m.DB.GetGuildString(context.Background(), ctx.Event.GuildID, "report:success")
	if err != nil || successMsg == "" {
		successMsg = defaultReportOK
	}

	errorMsg, err := m.DB.GetGuildString(context.Background(), ctx.Event.GuildID, "report:error")
	if err != nil || errorMsg == "" {
		errorMsg = defaultReportError
	}

	reason := ctx.Option("reason").String()
	if reason == "" {
		reason = "No reason given"
	}

	content := fmt.Sprintf(
		"New report by %v/%v from %v\nLink to context: <https://discord.com/channels/%v/%v/%v>",
		ctx.User.Mention(), ctx.User.Tag(), ctx.Event.ChannelID.Mention(),
		ctx.Event.GuildID, ctx.Event.ChannelID, discord.NewSnowflake(time.Now().UTC()),
	)

	_, err = m.State.SendMessageComplex(discord.ChannelID(chID), api.SendMessageData{
		Content: content,
		Embeds: []discord.Embed{{
			Title:       "Reason",
			Description: reason,
			Timestamp:   discord.NowTimestamp(),
			Footer: &discord.EmbedFooter{
				Text: "User ID: " + ctx.User.ID.String(),
			},
			Color: 0xc1140b,
		}},
		AllowedMentions: &api.AllowedMentions{},
	})
	if err != nil {
		m.Logger().Errorf("sending report message: %v", err)

		return ctx.ReplyEphemeral(errorMsg)
	}

	return ctx.ReplyEphemeral(successMsg)
}

func (m *Module) reportMsg(ctx *bcr.CommandContext) (err error) {
	chID, err := m.DB.GetGuildInt(context.Background(), ctx.Event.GuildID, "report:channel")
	if err != nil || chID == 0 {
		return ctx.ReplyEphemeral("This guild is not configured for reports.")
	}

	successMsg, err := m.DB.GetGuildString(context.Background(), ctx.Event.GuildID, "report:success")
	if err != nil || successMsg == "" {
		successMsg = defaultReportOK
	}

	errorMsg, err := m.DB.GetGuildString(context.Background(), ctx.Event.GuildID, "report:error")
	if err != nil || errorMsg == "" {
		errorMsg = defaultReportError
	}

	var msg *discord.Message
	for _, m := range ctx.Data.Resolved.Messages {
		msg = &m
	}

	if msg == nil {
		m.Logger().Debugf("interaction id %v did not contain a message", ctx.InteractionID)
		return ctx.ReplyEphemeral(errorMsg)
	}

	m.msgsMu.Lock()
	v, ok := m.msgs[msg.ID]
	m.msgsMu.Unlock()
	if ok {
		if v.Add(timeout).After(time.Now()) {
			return ctx.ReplyEphemeral(successMsg)
		}
	}

	content := fmt.Sprintf(
		"Message in %v reported by %v/%v\nLink to message: <https://discord.com/channels/%v/%v/%v>",
		ctx.Event.ChannelID.Mention(), ctx.User.Mention(), ctx.User.Tag(),
		ctx.Event.GuildID, msg.ChannelID, msg.ID,
	)

	_, err = m.State.SendMessageComplex(discord.ChannelID(chID), api.SendMessageData{
		Content: content,
		Embeds: []discord.Embed{{
			Author: &discord.EmbedAuthor{
				Icon: msg.Author.AvatarURLWithType(discord.PNGImage),
				Name: msg.Author.Tag() + " / " + msg.Author.ID.String(),
			},
			Description: msg.Content,
			Timestamp:   msg.Timestamp,
			Footer: &discord.EmbedFooter{
				Text: "Message ID: " + msg.ID.String(),
			},
			Color: 0xc1140b,
		}},
		AllowedMentions: &api.AllowedMentions{},
	})
	if err != nil {
		return ctx.ReplyEphemeral(errorMsg)
	}

	m.msgsMu.Lock()
	m.msgs[msg.ID] = time.Now()
	m.msgsMu.Unlock()

	return ctx.ReplyEphemeral(successMsg)
}

func (m *Module) configChannel(ctx *bcr.CommandContext) (err error) {
	sf, err := ctx.Option("channel").SnowflakeValue()
	if err != nil {
		return ctx.ReplyEphemeral("You didn't give a valid channel to use.")
	}

	ch, err := m.State.Channel(discord.ChannelID(sf))
	if err != nil || ch.GuildID != ctx.Event.GuildID {
		return ctx.ReplyEphemeral("That isn't a valid channel in this guild!")
	}

	err = m.DB.SetGuildInt(context.Background(), ctx.Event.GuildID, "report:channel", int64(ch.ID))
	if err != nil {
		m.Logger().Errorf("setting report channel: %v", err)

		return ctx.ReplyEphemeral("Error setting channel: " + err.Error())
	}

	return ctx.ReplyEphemeral("Report channel set to " + ch.Mention())
}

func (m *Module) configSuccessMessage(ctx *bcr.CommandContext) (err error) {
	msg := ctx.Option("message").String()

	err = m.DB.SetGuildString(context.Background(), ctx.Event.GuildID, "report:success", msg)
	if err != nil {
		m.Logger().Errorf("setting report success message: %v", err)

		return ctx.ReplyEphemeral("Error setting message: " + err.Error())
	}

	return ctx.ReplyEphemeral(fmt.Sprintf("Success message set to:\n```\n%v\n```", msg))
}

func (m *Module) configErrorMessage(ctx *bcr.CommandContext) (err error) {
	msg := ctx.Option("message").String()

	err = m.DB.SetGuildString(context.Background(), ctx.Event.GuildID, "report:error", msg)
	if err != nil {
		m.Logger().Errorf("setting report error message: %v", err)

		return ctx.ReplyEphemeral("Error setting message: " + err.Error())
	}

	return ctx.ReplyEphemeral(fmt.Sprintf("Error message set to:\n```\n%v\n```", msg))
}
